//Load the expressjs module into our application and save it in a variable called express.
const express = require('express');

//localhost port number
const port = 4000;

// app is our server
const app = express();

app.use(express.json());

//data
let items = [
  {
    name: 'Mjolnir',
    price: 50000,
    isActive: true,
  },
  {
    name: 'Vibranium Shield',
    price: 70000,
    isActive: true,
  },
];

// GET /home route
app.get('/home', (req, res) => {
  res.send('Welcome to the home page!');
});

app.get('/items', (req, res) => {
  res.json(items);
});

// DELETE /delete-item route
app.delete('/delete-item', (req, res) => {
  let deletedItem = items.pop();
  res.send(deletedItem);
});

app.listen(port, () => console.log('Server is running at port 4000'));
